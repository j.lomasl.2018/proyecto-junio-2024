import sqlite3
import requests
import time
from collections import defaultdict, deque
from flask import Flask, request, jsonify


DATABASE_NAME = "residencia.db"


def get_db():
    conn = sqlite3.connect(DATABASE_NAME)
    return conn


def create_tables():
    tables = [
        """
        CREATE TABLE IF NOT EXISTS Persona (
            id_usuario INTEGER PRIMARY KEY AUTOINCREMENT,
            id_microbit INTEGER,
            nombre VARCHAR(255),
            tipo VARCHAR(15),
            habitacion INTEGER,
            FOREIGN KEY (id_microbit) REFERENCES Microbit(id_microbit),
            FOREIGN KEY (habitacion) REFERENCES Sala(id_sala),
            CHECK (
                (tipo = 'usuario' AND habitacion IS NOT NULL) OR
                (tipo != 'usuario' AND habitacion IS NULL)
            )
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Sala (
            id_sala INTEGER PRIMARY KEY AUTOINCREMENT,
            nombre VARCHAR(255)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Puerta (
            id_puerta INTEGER PRIMARY KEY AUTOINCREMENT,
            id_microbit INTEGER,
            sala1 INTEGER,
            sala2 INTEGER,
            FOREIGN KEY (id_microbit) REFERENCES Microbit(id_microbit),
            FOREIGN KEY (sala1) REFERENCES Sala(id_sala),
            FOREIGN KEY (sala2) REFERENCES Sala(id_sala)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Acceso (
            id_acceso INTEGER PRIMARY KEY AUTOINCREMENT,
            usuario INTEGER NOT NULL,
            puerta INTEGER NOT NULL,
            fecha DATETIME DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY (usuario) REFERENCES Persona(id_usuario),
            FOREIGN KEY (puerta) REFERENCES Puerta(id_puerta)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Microbit (
            id_microbit INTEGER PRIMARY KEY AUTOINCREMENT,
            tipo VARCHAR(15),
            CHECK (
                tipo IN ('usuario', 'empleado', 'puerta', 'bs')
            )
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS PermisosPuertas (
            id_permiso INTEGER PRIMARY KEY AUTOINCREMENT,
            persona INTEGER NOT NULL,
            puerta INTEGER NOT NULL,
            FOREIGN KEY (persona) REFERENCES Persona(id_usuario)
            FOREIGN KEY (puerta) REFERENCES Puerta(id_puerta)
        )
        """,
    ]
    db = get_db()
    cursor = db.cursor()
    for table in tables:
        cursor.execute(table)
    db.commit()
    db.close()


def delete_database():
    try:
        db = sqlite3.connect("residencia.db")
        cursor = db.cursor()

        cursor.execute("DROP TABLE IF EXISTS Acceso")
        cursor.execute("DROP TABLE IF EXISTS Puerta")
        cursor.execute("DROP TABLE IF EXISTS Persona")
        cursor.execute("DROP TABLE IF EXISTS Sala")
        cursor.execute("DROP TABLE IF EXISTS Microbit")
        cursor.execute("DROP TABLE IF EXISTS PermisosPuertas")

        db.commit()
        print("Todas las tablas han sido eliminadas correctamente.")
    except sqlite3.Error as e:
        print("Error al eliminar las tablas:", e)
    finally:
        db.close()


def add_user(user_name, type, id_room=None):
    if (type == "empleado" and id_room) or (type == "usuario" and not id_room):
        print("Invalid input. User must have room, else None.")
        return

    try:
        db = get_db()
        cursor = db.cursor()

        db.execute("BEGIN")

        # Inserta un nuevo registro en la tabla Microbit y obtén el id_microbit generado
        cursor.execute("INSERT INTO Microbit (tipo) VALUES (?)", (type,))
        id_microbit = cursor.lastrowid  # Obtiene el último id_microbit insertado

        # Inserta el usuario con el id_microbit asociado
        cursor.execute(
            "INSERT INTO Persona (id_microbit, nombre, tipo, habitacion) VALUES (?, ?, ?, ?)",
            (id_microbit, user_name, type, id_room),
        )

        # Confirma la transacción
        db.execute("COMMIT")

        if type == "usuario":
            print(
                f"Añadido usuario {user_name} con habitación {id_room} y microbit {id_microbit}"
            )
        else:
            print(f"Añadido empleado {user_name} con microbit {id_microbit}")

    except Exception as e:
        # Si hay algún error se deshace todo
        db.execute("ROLLBACK")
        print("Error al añadir usuario:", e)
    finally:
        db.close()


def add_room(room_name):
    try:
        db = get_db()

        cursor = db.cursor()
        cursor.execute("INSERT INTO Sala (nombre) VALUES (?)", (room_name,))
        db.commit()
        print(f"Añadida sala '{room_name}'")
    except Exception as e:
        print("Error al añadir sala:", e)
    finally:
        db.close()


def add_door(name_room1, name_room2):
    try:
        db = get_db()
        cursor = db.cursor()

        # Comienza una transacción
        db.execute("BEGIN")

        # Inserta un nuevo registro en la tabla Microbit y obtén el id_microbit generado
        cursor.execute("INSERT INTO Microbit (tipo) VALUES ('puerta')")
        id_microbit = cursor.lastrowid  # Obtiene el último id_microbit insertado

        # Inserta la puerta con el id_microbit asociado
        cursor.execute(
            """
            INSERT INTO Puerta (sala1, sala2, id_microbit)
            SELECT s1.id_sala, s2.id_sala, ?
            FROM Sala AS s1
            INNER JOIN Sala AS s2 ON s1.nombre = ? AND s2.nombre = ?
            """,
            (id_microbit, name_room1, name_room2),
        )

        # Confirma la transacción
        db.execute("COMMIT")

        print(
            f"Añadida puerta: {name_room1} <-> {name_room2} con microbit {id_microbit}"
        )

    except Exception as e:
        # Si ocurre un error, realiza un rollback para deshacer cualquier cambio en la transacción
        db.execute("ROLLBACK")
        print("Error al añadir puerta:", e)
    finally:
        db.close()


def add_acceso(id_user, id_door):
    try:
        db = get_db()

        cursor = db.cursor()
        cursor.execute(
            "INSERT INTO Acceso (usuario, puerta) VALUES (?, ?)", (id_user, id_door)
        )
        db.commit()
        print(f"Registrado acceso de usuario {id_user} a la puerta {id_door}")
    except Exception as e:
        print("Error al registrar acceso de usuario:", e)
    finally:
        db.close()


def init_test_db():
    rooms = [
        {
            "name": "H1",
        },
        {
            "name": "H2",
        },
        {
            "name": "H3",
        },
        {
            "name": "P",
        },
        {
            "name": "Principal",
        },
        {
            "name": "Hall",
        },
    ]

    doors = [
        {"room1": "H1", "room2": "P"},
        {"room1": "H2", "room2": "P"},
        {"room1": "H3", "room2": "P"},
        {"room1": "P", "room2": "Principal"},
        {"room1": "Principal", "room2": "Hall"},
        {"room1": "P", "room2": "Hall"},
    ]

    users = [
        {"name": "Emilio Torrevieja", "room": 1},
        {"name": "Clara Villalba", "room": 2},
        {"name": "Teresa Gómez", "room": 3},
    ]

    empleados = [{"name": "Lucía Madariaga"}, {"name": "Rubén García"}]

    accesses = [
        # Persona 1 -> H1 - P - Principal - Hall
        {"user": 1, "door": 1},
        {"user": 1, "door": 4},
        {"user": 1, "door": 5},
        # Persona 2 -> H2 - P - Hall - Principal - P - H2
        {"user": 2, "door": 2},
        {"user": 2, "door": 6},
        {"user": 2, "door": 5},
        {"user": 2, "door": 4},
        {"user": 2, "door": 2},
        # Persona 3 -> H3 - P - Principal - P - H3
        {"user": 3, "door": 3},
        {"user": 3, "door": 4},
        {"user": 3, "door": 4},
        {"user": 3, "door": 3},
        # Persona 4 (empleado) -> Hall - Principal - P - Hall
        {"user": 4, "door": 5},
        {"user": 4, "door": 4},
        {"user": 4, "door": 6},
        # Persona 5 (empleado) -> Hall - P - Principal - P - H2
        {"user": 5, "door": 6},
        {"user": 5, "door": 4},
        {"user": 5, "door": 4},
        {"user": 5, "door": 2},
    ]

    for r in rooms:
        add_room(r["name"])

    for d in doors:
        add_door(d["room1"], d["room2"])

    for u in users:
        add_user(u["name"], "usuario", u["room"])

    for e in empleados:
        add_user(e["name"], "empleado")

    for a in accesses:
        add_acceso(a["user"], a["door"])
        time.sleep(1)


def get_last_doors(id_user):
    db = get_db()
    last_two_doors = []

    try:
        cursor = db.cursor()

        cursor.execute(
            """
            SELECT Acceso.puerta AS id_puerta, 
                Puerta.sala1 AS sala1_id, 
                Sala1.nombre AS sala1_nombre, 
                Puerta.sala2 AS sala2_id, 
                Sala2.nombre AS sala2_nombre
            FROM Acceso
            INNER JOIN Puerta ON Acceso.puerta = Puerta.id_puerta
            INNER JOIN Sala AS Sala1 ON Puerta.sala1 = Sala1.id_sala
            INNER JOIN Sala AS Sala2 ON Puerta.sala2 = Sala2.id_sala
            WHERE Acceso.usuario = ?
            ORDER BY Acceso.fecha DESC
            LIMIT 2;
            """,
            (id_user,),
        )

        access_results = cursor.fetchall()

        for row in access_results:
            door_info = {
                "id_puerta": row[0],
                "sala1_id": row[1],
                "sala1_nombre": row[2],
                "sala2_id": row[3],
                "sala2_nombre": row[4],
            }

            last_two_doors.append(door_info)

    except sqlite3.Error as e:
        print("Error en la consulta SQL: " + str(e))
        return None

    finally:
        db.close()

    return last_two_doors


def main():
    delete_database()
    create_tables()
    init_test_db()

if __name__ == "__main__":
    main()
