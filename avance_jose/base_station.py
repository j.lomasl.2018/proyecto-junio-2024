# CODIGO DEL MICROBIT DE LA BASE STATION

from microbit import *
import radio
import music
import random
import utime

radio.on()
radio.config(channel=5)  # Configura el canal de radio (el mismo en ambos micro:bit)

ID_MB = 0  # Inicia el id en 0
destino = 0
received_messages = []  # Lista de strings
sent_messages = []  # Lista de strings
received_data = ""  # Para procesar mensajes con BS


def generate_msg_id():
    # Genera un número aleatorio de 6 dígitos
    msg_id = random.randint(100000, 999999)
    return msg_id


def send_message(id_msg, from_id, to_id, content):
    """
    Envía mensajes por radio a otros microbit
    """
    message = "{} {} {} {}".format(id_msg, from_id, to_id, content)
    sent_messages.append(message)
    radio.send(message)


# Aquí ya hacemos lo que queramos
def process_action(msg):
    """
    Gestiona las acciones a realizar el función del mensaje recibido
    """
    data = msg.split(" ")
    if len(data) < 4:  # Para evitar posibles errores de entrada
        return
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    display.scroll(msg, delay=75)
    
    if content[:12] == "OPEN_SUCCESS":
        write_to_bs(msg)
    elif content == "ALERT":
        write_to_bs(msg)
    elif content[:4] == "HELP":
        send_message(generate_msg_id(), from_id, to_id, content)
    elif content[:3] == "ADD":
        send_message(generate_msg_id(), from_id, to_id, content)
        for i in range(3):
            display.scroll(content, delay=75)
    elif content[:3] == "DEL":
        send_message(generate_msg_id(), from_id, to_id, content)
        for i in range(3):
            display.scroll(content, delay=75)
    pass


def communicate(msg):
    """
    Gestiona las comunicaciones entre dispositivos
    """
    global ID_MB
    global received_messages
    global sent_messages
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    # Si ya hemos enviado este mensaje o ya lo hemos recibido no hacemos nada
    if from_id == ID_MB or msg in received_messages or (content[0:4] == "OPEN" and content[:12] != "OPEN_SUCCESS"):
        return
    # Soy el destinatario
    if to_id == ID_MB:
        # Si el mensaje no es ACK es original => Envío ACK
        if content[0:3] is not "ACK":
            process_action(msg)
            send_message(generate_msg_id(), to_id, from_id, "ACK_" + str(id_msg))
        # Recibimos un ACK, así que borramos el mensaje de la lista de enviados
        else:
            id = content[4:]
            for m in sent_messages:
                if id == m[0:7]:
                    sent_messages.remove(m)
                    break
        return
    if content[0:3] is not "ACK":
        # Recibo un mensaje normal, pero ya he recibido su ACK
        for m in received_messages:
            if "ACK" == m[-10:-7] and id_msg == m[-6:]:
                return

    received_messages.append(msg)  # Guardar mensaje recibido
    send_message(id_msg, from_id, to_id, content)  # Propagamos


def check_input():  # Método temporal para testear
    global ID_MB
    global destino
    if button_a.was_pressed():
        destino += 1
        if destino == 13:
            destino = 0
        display.scroll(str(destino), delay=50)
    elif button_b.was_pressed():
        ID_MB += 1
        if ID_MB == 13:
            ID_MB = 0
        display.scroll(str(ID_MB), delay=50)


def read_from_bs():
    """
    Lee y gestiona los datos enviados por la BS
    """
    global received_data

    # Construimos el mensaje a mostrar
    msg = uart.read()
    if msg is not None:
        process_action(str(msg, "UTF-8"))  # La BS nos manda una orden con el mismo formato
        received_data = ""


def write_to_bs(message):
    message = str(message, "UTF-8")
    uart.write(message)
        

def main():
    uart.init(baudrate=115200)
    while True:
        check_input()
        read_from_bs()
        message = radio.receive()  # Espera a recibir un mensaje
        if message:
            communicate(message)


if __name__ == "__main__":
    main()

