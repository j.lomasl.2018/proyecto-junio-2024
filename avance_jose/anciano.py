# CODIGO DEL MICROBIT DEL ANCIANO

from microbit import *
import radio
import music
import random

radio.on()
radio.config(channel=5, power=1)  # Configura el canal de radio

ID_MB = 0  # Inicia el id en 0
received_messages = []  # Lista de strings
sent_messages = []  # Listade strings
DOORS = set()  # Set de puertas que puedo abrir
DOORS.add(5)
DOORS.add(1)


def generate_msg_id():
    # Genera un número aleatorio de 6 dígitos
    msg_id = random.randint(100000, 999999)
    return msg_id


def send_message(id_msg, from_id, to_id, content):
    message = "{} {} {} {}".format(id_msg, from_id, to_id, content)
    print(message)
    sent_messages.append(message)
    radio.send(message)


def process_action(msg):
    data = msg.split(" ")
    # id_msg = int(data[0])
    from_id = int(data[1])
    # to_id = int(data[2])
    content = data[3]
    if content[:3] == "ADD":
        music.play(music.BA_DING, wait=False)
        DOORS.add(int(content[4:]))
    elif content[:3] == "DEL" and int(content[4:]) in DOORS:
        music.play(music.BA_DING, wait=False)
        DOORS.remove(int(content[4:]))
    elif content == "OPENRES":
        open_door(from_id)


def communicate(msg):
    global ID_MB
    global received_messages
    global sent_messages
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    # Si ya hemos enviado este mensaje o ya lo hemos recibido no hacemos nada.
    # Si es un mensaje de abrir puerta lo ignoramos,
    # es la puerta el encargado de enviarlo.
    if (
        from_id == ID_MB
        or msg in received_messages
        or (
            content[:4] == "OPEN" and to_id != ID_MB and content[:12] != "OPEN_SUCCESS"
        )
    ):
        return
    # Soy el destinatario
    if to_id == ID_MB:  # Si el mensaje no es ACK es original => Envío ACK
        if content[:3] != "ACK":
            process_action(msg)
            send_message(generate_msg_id(), to_id, from_id, "ACK_" + str(id_msg))
        # Recibimos un ACK, así que borramos el mensaje de la lista de enviados
        else:
            id = content[4:]
            for m in sent_messages:
                if id == m[:6]:
                    sent_messages.remove(m)
                    break
        return
    if content[:3] != "ACK":
        # Recibo un mensaje normal, pero ya he recibido su ACK
        for m in received_messages:
            if "ACK" == m[-10:-7] and id_msg == m[-6:]:
                return
    received_messages.append(msg)  # Guardar mensaje recibido
    send_message(id_msg, from_id, to_id, content)  # Propagamos


def check_input():  # Método temporal para testear
    global ID_MB
    # Para enviar solicitud de abrir puerta
    if button_a.was_pressed():
        send_message(generate_msg_id(), ID_MB, 0, "OPENREQ")
        display.show(Image.YES)
        sleep(500)
        display.clear()
    # Para cambiar el id de este microbit (puede ir hardcoded)
    elif button_b.was_pressed():
        ID_MB += 1
        if ID_MB == 13:
            ID_MB = 0
        display.scroll(str(ID_MB), delay=50)
    elif mb_shaken():
        send_message(generate_msg_id(), ID_MB, 0, "HELP")
        display.scroll("HELP", delay=50)
        sleep(1000)


def mb_shaken():
    umbral = 3000
    tiempo_inicio = running_time()

    x_anterior = accelerometer.get_x()
    y_anterior = accelerometer.get_y()
    z_anterior = accelerometer.get_z()

    while running_time() - tiempo_inicio < 1000:
        x_actual = accelerometer.get_x()
        y_actual = accelerometer.get_y()
        z_actual = accelerometer.get_z()

        cambio_x = abs(x_actual - x_anterior)
        cambio_y = abs(y_actual - y_anterior)
        cambio_z = abs(z_actual - z_anterior)

        if cambio_x > umbral or cambio_y > umbral or cambio_z > umbral:
            return True

        x_anterior = x_actual
        y_anterior = y_actual
        z_anterior = z_actual

    return False


def open_door(id_door):
    if id_door in DOORS:
        music.pitch(350, 100, pin0, True)
        display.show(Image.YES)
        sleep(1000)
        display.clear()
        send_message(generate_msg_id(), ID_MB, id_door, "OPEN")
    else:
        music.pitch(350, 100, pin0, True)
        display.show(Image.NO)
        sleep(1000)
        display.clear()


def main():
    while True:
        check_input()
        message = radio.receive()  # Espera a recibir un mensaje
        if message:
            communicate(message)


if __name__ == "__main__":
    main()
