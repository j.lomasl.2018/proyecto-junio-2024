from flask import Flask, jsonify, request
import requests
import serial
import json
import serial.tools.list_ports as list_ports
from threading import Thread

# CONSTANTES
PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 10
PORT_RESIDENCIA = 5000
PORT = 1234

app = Flask(__name__)


def find_comport(pid, vid, baud):
    ser_port = serial.Serial(timeout=TIMEOUT)
    ser_port.baudrate = baud
    ports = list(list_ports.comports())
    print('scanning ports')
    for p in ports:
        print('port: {}'.format(p))
        try:
            print('pid: {} vid: {}'.format(p.pid, p.vid))
        except AttributeError:
            continue
        if (p.pid == pid) and (p.vid == vid):
            print('found target device pid: {} vid: {} port: {}'.format(
                p.pid, p.vid, p.device))
            ser_port.port = str(p.device)
            return ser_port
    return None


def receive_microbit_msg(period, msg):
    micro_bit = msg
    while True:
        lin = type(micro_bit.readline())
        line = micro_bit.readline().decode('utf-8')
        if line:
            new_line = line.strip()
            print(new_line)
            codigo = line.split(" ")[3]
            print("cod: " + codigo)
            if "OPEN" in new_line:
                ans = {"mb_user": line.split(" ")[1], "door": line.split(" ")[2]}
                msg_microbit = json.dumps(ans)
                URL = "http://localhost:" + str(PORT_RESIDENCIA) + "/store-access"
                headers = {
                    "Content-Type": "application/json"
                }
                print("Mensaje: " + msg_microbit)
                peticion = requests.post(URL, headers=headers, data=msg_microbit)
            elif "HELP" in new_line:
                ans = {"mb_user": line.split(" ")[1]}
                msg_microbit = json.dumps(ans)
                URL = "http://localhost:" + str(PORT_RESIDENCIA) + "/help"
                headers = {
                    "Content-Type": "application/json"
                }
                print("Mensaje: " + msg_microbit)
                peticion = requests.get(URL, headers=headers, data=msg_microbit)



if __name__ == "__main__":
    global micro_bit
    micro_bit = find_comport(PID_MICROBIT, VID_MICROBIT, 9760)
    micro_bit.open()
    t = Thread(target=receive_microbit_msg, args=[1, micro_bit])
    t.start()
    #app.run(host='0.0.0.0', port=PORT, debug=False)
    app.run(debug=False, host='127.0.0.1', port=PORT)
    micro_bit.close()
