------------------------ MODULE enfermero ------------------------
EXTENDS Integers, Sequences

CONSTANTS ID_MB, help_delay

VARIABLES
    destino, received_messages, sent_messages, last_help_time, DOORS

(* -- Algorithm variables initialization -- *)
Init == 
    /\ destino = 0
    /\ received_messages = <<>>
    /\ sent_messages = <<>>
    /\ DOORS = {}
    /\ last_help_time = 0

(* -- Message ID generation -- *)
GenerateMsgID == CHOOSE id_msg \in 100000..999999 : TRUE

(* -- Message sending -- *)
Send(message) == 
    /\ sent_messages' = Append(sent_messages, message)
    /\ UNCHANGED <<destino, received_messages, last_help_time, DOORS>>

(* -- Processing message actions -- *)
ProcessAction(msg) ==
    LET 
        data == Split(msg, " ") 
        content == data[3]
    IN
        IF content[0..2] = "ADD" THEN
            /\ DOORS' = DOORS \cup {ToInt(SubSeq(content, 4, Len(content) - 3))}
            /\ UNCHANGED <<sent_messages, received_messages, destino, last_help_time>>
        ELSE IF content[0..2] = "DEL" THEN
            /\ DOORS' = DOORS \ {ToInt(SubSeq(content, 4, Len(content) - 3))}
            /\ UNCHANGED <<sent_messages, received_messages, destino, last_help_time>>
        ELSE IF content[0..3] = "HELP" THEN
            /\ IF (RunningTime() - last_help_time) > help_delay THEN
                /\ last_help_time' = RunningTime()
                /\ UNCHANGED <<sent_messages, received_messages, destino, DOORS>>
            ELSE
                /\ UNCHANGED <<sent_messages, received_messages, destino, last_help_time, DOORS>>

(* -- Communication protocol -- *)
Communicate(msg) ==
    LET 
        data == Split(msg, " ")
        id_msg == ToInt(data[0])
        from_id == ToInt(data[1])
        to_id == ToInt(data[2])
        content == data[3]
    IN
        IF from_id = ID_MB \/ msg \in received_messages \/
           (content[0..3] = "OPEN" /\ to_id # ID_MB /\ content[0..11] # "OPEN_SUCCESS") THEN
            /\ UNCHANGED <<sent_messages, received_messages, destino, last_help_time, DOORS>>
        ELSE IF to_id = ID_MB THEN
            /\ IF content[0..2] # "ACK" THEN
                /\ ProcessAction(msg)
                /\ Send(Concatenate(<<GenerateMsgID(), to_id, from_id, "ACK_", ToString(id_msg)>>))
                /\ UNCHANGED <<received_messages, destino, last_help_time, DOORS>>
            ELSE
                /\ LET id == SubSeq(content, 4, Len(content) - 3)
                   IN
                   /\ sent_messages' = [m \in sent_messages : m[0..5] # id]
                   /\ UNCHANGED <<received_messages, destino, last_help_time, DOORS>>
        ELSE
            /\ IF content[0..2] # "ACK" THEN
                /\ LET ackMsg == [m \in received_messages : 
                                 "ACK" = SubSeq(m, Len(m) - 9, Len(m) - 7) /\ id_msg = SubSeq(m, Len(m) - 5, Len(m))]
                   IN
                   /\ UNCHANGED <<sent_messages, received_messages, destino, last_help_time, DOORS>>
        /\ received_messages' = Append(received_messages, msg)

(* -- Input checking -- *)
CheckInput() ==
    /\ IF button_a.was_pressed() THEN
        /\ destino' = IF destino = 12 THEN 0 ELSE destino + 1
        /\ UNCHANGED <<sent_messages, received_messages, last_help_time, DOORS>>
    ELSE IF button_b.was_pressed() THEN
        /\ ID_MB' = IF ID_MB = 12 THEN 0 ELSE ID_MB + 1
        /\ UNCHANGED <<destino, sent_messages, received_messages, last_help_time, DOORS>>
    ELSE
        /\ UNCHANGED <<ID_MB, destino, sent_messages, received_messages, last_help_time, DOORS>>

(* -- Sound alert -- *)
AlertSound() ==
    LET freq == 1850,
        duration == 200
    IN
        /\ FOR i \in 1..2 DO
            /\ PlaySound(freq, duration, pin0)
            /\ UNCHANGED <<ID_MB, destino, sent_messages, received_messages, last_help_time, DOORS>>

(* -- Main algorithm loop -- *)
Main == 
    /\ while TRUE do
        /\ CheckInput()
        /\ IF message \in received_messages THEN
            /\ ProcessAction(message)
            /\ Send(message)
            /\ UNCHANGED <<destino, last_help_time, DOORS>>
        /\ ELSE
            /\ UNCHANGED <<ID_MB, destino, sent_messages, received_messages, last_help_time, DOORS>>
    /\ UNCHANGED <<ID_MB, destino, sent_messages, received_messages, last_help_time, DOORS>>

Spec == Init /\ [][Main]_<<ID_MB, destino, sent_messages, received_messages, last_help_time, DOORS>>
=====================================================================

