---- MODULE puerta ----
EXTENDS Integers

CONSTANT MAX_ID = 12  \* Máximo ID de micro:bit, de 0 a 12

VARIABLE ID_MB, received_messages, sent_messages

(* Operaciones y funciones auxiliares *)

GenerateMsgID == RandomElement({100000..999999})

Send(message) == 
    /\ sent_messages' = Append(sent_messages, message)
    /\ UNCHANGED <<ID_MB, received_messages>>

ProcessAction(message) ==
    LET data == Split(message, " ") 
        id_msg == ToInt(data[1])
        from_id == ToInt(data[2])
        to_id == ToInt(data[3])
        content == data[4]
    IN
        CASE content = "OPENREQ" -> HandleOpenRequest(from_id)
       [] content = "OPEN" /\ Len(data) = 5 -> Send(GenerateMsgID() \o " " \o ToString(ID_MB) \o " 0 " \o "OPEN_SUCCESS_" \o ToString(from_id))
       [] content = "HELP" -> Send(message)

HandleOpenRequest(id_user) == Send(GenerateMsgID() \o " " \o ToString(ID_MB) \o " " \o ToString(id_user) \o " OPENRES")

Communicate(message) ==
    LET data == Split(message, " ")
        id_msg == ToInt(data[1])
        from_id == ToInt(data[2])
        to_id == ToInt(data[3])
        content == data[4]
    IN
        IF from_id = ID_MB \/ message \in received_messages THEN
            Skip
        ELSE IF to_id = ID_MB \/ to_id = 0 THEN
            IF Substring(content, 0, 3) /= "ACK" THEN
                ProcessAction(message);
                Send(GenerateMsgID() \o " " \o ToString(ID_MB) \o " " \o ToString(from_id) \o " " \o "ACK_" \o ToString(id_msg))
            ELSE
                LET ack_id == ToInt(Substring(content, 4, Len(content)))
                IN sent_messages' = [m \in sent_messages | ToInt(Split(m, " ")[1]) /= ack_id]
        ELSE
            IF Substring(content, 0, 3) /= "ACK" THEN
                IF \E m \in received_messages : "ACK" \in m /\ id_msg = ToInt(Split(m, " ")[1]) THEN
                    Skip
                ELSE
                    received_messages' = Append(received_messages, message);
                    Send(message)
            ELSE
                Skip

CheckInput ==
    IF button_a.was_pressed() THEN
        Print(ToString(ID_MB))
    ELSE IF button_b.was_pressed() THEN
        ID_MB' = IF ID_MB = MAX_ID THEN 0 ELSE ID_MB + 1

Main ==
    /\ ID_MB' = ID_MB
    /\ \A r \in received_messages : Len(r) > 0
    /\ \A s \in sent_messages : Len(s) > 0

Init ==
    /\ ID_MB \in 0..MAX_ID
    /\ received_messages = <<>>
    /\ sent_messages = <<>>

Next ==
    \/ CheckInput
    \/ \E message \in {m \in received_messages : Len(m) > 0} : Communicate(message)

Spec == Init /\ [][Next]_<<ID_MB, received_messages, sent_messages>>
    
====


