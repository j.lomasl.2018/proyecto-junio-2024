------------------------ MODULE anciano ------------------------
EXTENDS Integers, Sequences

CONSTANTS ID_MB

VARIABLES
    received_messages, sent_messages, DOORS

(* Initial state *)
Init ==
    /\ received_messages = <<>>
    /\ sent_messages = <<>>
    /\ DOORS = {5, 1}

(* Message ID generation *)
GenerateMsgID ==
    CHOOSE id_msg \in 100000..999999 : TRUE

(* Sending a message *)
Send(message) ==
    /\ sent_messages' = Append(sent_messages, message)
    /\ UNCHANGED <<received_messages, DOORS>>

(* Processing message actions *)
ProcessAction(msg) ==
    LET
        data == Split(msg, " ")
        from_id == ToInt(data[1])
        content == data[3]
    IN
        IF Len(data) < 4 \/ (content[0..4] # "OPEN" /\ content # "HELP") THEN
            /\ UNCHANGED <<sent_messages, received_messages, DOORS>>
        ELSE
            /\ IF content = "OPENRES" THEN
                /\ OpenDoor(from_id)
               ELSE
                /\ UNCHANGED DOORS
            /\ Send(Concatenate(<<GenerateMsgID(), data[1], data[2], content>>))

(* Communicating messages *)
Communicate(msg) ==
    LET
        data == Split(msg, " ")
        from_id == ToInt(data[1])
        to_id == ToInt(data[2])
        content == data[3]
    IN
        IF from_id = ID_MB \/ msg \in received_messages \/ content[0..3] # "OPEN" THEN
            /\ UNCHANGED <<sent_messages, DOORS>>
        ELSE IF to_id = ID_MB THEN
            /\ ProcessAction(msg)
        /\ received_messages' = Append(received_messages, msg)

(* Checking input *)
CheckInput() ==
    /\ IF button_a.was_pressed() THEN
        /\ Send(Concatenate(<<GenerateMsgID(), ID_MB, 0, "OPENREQ">>))
        /\ DisplayYes()
    ELSE IF button_b.was_pressed() THEN
        /\ ID_MB' = IF ID_MB = 12 THEN 0 ELSE ID_MB + 1
    ELSE IF mb_shaken() THEN
        /\ Send(Concatenate(<<GenerateMsgID(), ID_MB, 0, "HELP">>))
        /\ DisplayHelp()

(* Shaking detection *)
mb_shaken() ==
    LET
        threshold == 3000
        start_time == GetRunningTime()
        (x_prev, y_prev, z_prev) == (GetAccX(), GetAccY(), GetAccZ())
    IN
        /\ WHILE GetRunningTime() - start_time < 1000 DO
            /\ (x_curr, y_curr, z_curr) == (GetAccX(), GetAccY(), GetAccZ())
            /\ IF Abs(x_curr - x_prev) > threshold \/ Abs(y_curr - y_prev) > threshold \/ Abs(z_curr - z_prev) > threshold THEN
                /\ RETURN TRUE
            /\ x_prev, y_prev, z_prev := x_curr, y_curr, z_curr
        /\ RETURN FALSE

(* Opening a door *)
OpenDoor(id_door) ==
    IF id_door \in DOORS THEN
        /\ SoundYes()
        /\ Send(Concatenate(<<GenerateMsgID(), ID_MB, id_door, "OPEN">>))
    ELSE
        /\ SoundNo()

(* Main algorithm loop *)
Main ==
    /\ while TRUE do
        /\ CheckInput()
        /\ message \in received_messages => Communicate(message)
        /\ UNCHANGED <<ID_MB, sent_messages, DOORS>>

Spec == Init /\ [][Main]_<<ID_MB, sent_messages, received_messages, DOORS>>
=====================================================================

