------------------------ MODULE basestation ------------------------
EXTENDS Integers, Sequences

CONSTANTS ID_MB

VARIABLES
    destino, received_messages, sent_messages, received_data

(* -- Algorithm variables initialization -- *)
Init == 
    /\ destino = 0
    /\ received_messages = <<>>
    /\ sent_messages = <<>>
    /\ received_data = ""

(* -- Message ID generation -- *)
GenerateMsgID == CHOOSE id_msg \in 100000..999999 : TRUE

(* -- Message sending -- *)
Send(message) == 
    /\ sent_messages' = Append(sent_messages, message)
    /\ UNCHANGED <<destino, received_messages, received_data>>

(* -- Processing message actions -- *)
ProcessAction(msg) ==
    LET 
        data == Split(msg, " ") 
        content == data[3]
    IN
        IF Len(data) < 4 \/ (content[0..11] # "OPEN_SUCCESS" /\ content # "ALERT" /\ 
                             content[0..3] # "HELP" /\ content[0..2] # "ADD" /\ content[0..2] # "DEL") THEN
            /\ UNCHANGED <<sent_messages, destino, received_messages, received_data>>
        ELSE
            /\ IF content[0..11] = "OPEN_SUCCESS" \/ content = "ALERT" THEN
                /\ WriteToBS(msg)
            ELSE
                /\ Send(Concatenate(<<GenerateMsgID(), data[1], data[2], content>>))
        /\ UNCHANGED <<sent_messages, destino, received_messages, received_data>>

(* -- Communication protocol -- *)
Communicate(msg) ==
    LET 
        data == Split(msg, " ")
        id_msg == ToInt(data[0])
        from_id == ToInt(data[1])
        to_id == ToInt(data[2])
        content == data[3]
    IN
        IF from_id = ID_MB \/ msg \in received_messages \/ 
           (content[0..4] = "OPEN" /\ content[0..12] # "OPEN_SUCCESS") THEN
            /\ UNCHANGED <<sent_messages, destino, received_data>>
        ELSE IF to_id = ID_MB THEN
            /\ IF content[0..2] # "ACK" THEN
                /\ ProcessAction(msg)
                /\ Send(Concatenate(<<GenerateMsgID(), to_id, from_id, "ACK_", ToString(id_msg)>>))
                /\ UNCHANGED <<destino, received_messages, received_data>>
            ELSE
                /\ LET id == SubSeq(content, 4, Len(content) - 3)
                   IN
                   /\ sent_messages' = [m \in sent_messages : m[0..6] # id]
                   /\ UNCHANGED <<destino, received_messages, received_data>>
        ELSE
            /\ IF content[0..2] # "ACK" THEN
                /\ LET ackMsg == [m \in received_messages : 
                                 "ACK" = SubSeq(m, Len(m) - 9, Len(m) - 7) /\ id_msg = SubSeq(m, Len(m) - 5, Len(m))]
                   IN
                   /\ UNCHANGED <<sent_messages, destino, received_data>>
        /\ received_messages' = Append(received_messages, msg)

(* -- Input checking -- *)
CheckInput() ==
    /\ IF button_a.was_pressed() THEN
        /\ destino' = IF destino = 12 THEN 0 ELSE destino + 1
        /\ UNCHANGED <<sent_messages, received_messages, received_data>>
    ELSE IF button_b.was_pressed() THEN
        /\ ID_MB' = IF ID_MB = 12 THEN 0 ELSE ID_MB + 1
        /\ UNCHANGED <<destino, sent_messages, received_messages, received_data>>
    ELSE
        /\ UNCHANGED <<ID_MB, destino, sent_messages, received_messages, received_data>>

(* -- Read from Base Station -- *)
ReadFromBS() ==
    LET msg == uart.read()
    IN
        /\ IF msg \in BOOLEAN THEN
            /\ ProcessAction(ToString(msg, "UTF-8"))
            /\ received_data' = ""
        ELSE
            /\ UNCHANGED <<sent_messages, destino, received_messages, ID_MB>>

(* -- Write to Base Station -- *)
WriteToBS(msg) ==
    LET message == ToString(msg, "UTF-8")
    IN
        /\ uart.write(message)
        /\ UNCHANGED <<destino, received_messages, sent_messages, received_data>>

(* -- Main algorithm loop -- *)
Main == 
    /\ while TRUE do
        /\ CheckInput()
        /\ ReadFromBS()
        /\ message \in received_messages => Communicate(message)
        /\ UNCHANGED <<ID_MB, destino, sent_messages, received_messages, received_data>>

Spec == Init /\ [][Main]_<<ID_MB, destino, sent_messages, received_messages, received_data>>
=====================================================================

