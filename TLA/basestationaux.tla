----------------------------- MODULE basestationaux -----------------------------

EXTENDS Integers, Sequences

CONSTANTS
    PID_MICROBIT,
    VID_MICROBIT,
    TIMEOUT,
    PORT_RESIDENCIA,
    PORT

VARIABLES
    micro_bit

(* -- Algorithm for finding the Microbit device -- *)
FindCOMPort(pid, vid, baud) ==
    LET ports == {p \in DOMAIN ListPorts : p.pid = pid /\ p.vid = vid}
    IN  IF ports = {} THEN
            NULL
        ELSE
            CHOOSE p \in ports : TRUE

(* -- Specification of receive_microbit_msg function -- *)
ReceiveMicrobitMsg(period, msg) ==
    /\ \* Simulate the behavior of receiving messages from Microbit
    /\ \* Interact with Flask endpoints based on message content

(* -- Initial state and system operations -- *)
Init ==
    /\ micro_bit = FindCOMPort(PID_MICROBIT, VID_MICROBIT, 9760)  \* Example baud rate

Next ==
    \/ \E line \in String :
        /\ line \in ReceiveMicrobitMsg(Next)
        /\ UNCHANGED <<otros_valores_del_estado>>

Spec ==
    /\ Init
    /\ [][Next]_micro_bit

===============================================================================

