---- MODULE residencia ----
EXTENDS Integers

CONSTANT 
	MAX_ROOMS,
	MAX_DOORS,
	MAX_USERS

VARIABLE rooms, doors, users, accessRecords

Init == 
    /\ rooms = {}
    /\ doors = {}
    /\ users = {}
    /\ accessRecords = <<>>

AddRoom(name) == 
    rooms' = rooms \cup {name}

AddDoor(room1, room2) == 
    doors' = doors \cup {<<room1, room2>>}

AddUser(name, type, room) == 
    users' = users \cup {<<name, type, room>>}

AddAccess(user, door) == 
    accessRecords' = Append(accessRecords, <<user, door, Now()>>)

Spec == 
    Init /\ [][Next]_<<rooms, doors, users, accessRecords>>

Next == 
    \E name \in {"AddRoom", "AddDoor", "AddUser", "AddAccess"}:
        CASE name = "AddRoom" => 
            \E roomName \in 1..MAX_ROOMS: AddRoom(roomName)
        [] name = "AddDoor" =>
            \E room1 \in 1..MAX_ROOMS, room2 \in 1..MAX_ROOMS: AddDoor(room1, room2)
        [] name = "AddUser" =>
            \E userName \in {"User1", "User2", "User3"}, userType \in {"usuario", "empleado"}:
                \E roomNum \in 1..MAX_ROOMS: AddUser(userName, userType, roomNum)
        [] name = "AddAccess" =>
            \E userNum \in 1..MAX_USERS, doorNum \in 1..MAX_DOORS: AddAccess(userNum, doorNum)
====

