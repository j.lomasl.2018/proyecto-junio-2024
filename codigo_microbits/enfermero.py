# CODIGO DEL MICROBIT DEL ENFERMERO

from microbit import *
import radio
import music
import random

radio.on()
radio.config(channel=5, power=2)  # Ajusta la potencia de transmisión

ID_MB = 0  # Inicia el id en 0
destino = 0
received_messages = []  # Lista de strings
sent_messages = []  # Lista de strings
DOORS = set()

def generate_msg_id():
    msg_id = random.randint(100000, 999999)
    return msg_id

def send_message(id_msg, from_id, to_id, content):
    message = "{} {} {} {}".format(id_msg, from_id, to_id, content)
    sent_messages.append(message)
    radio.send(message)

def process_action(msg):
    data = msg.split(" ")
    # id_msg = int(data[0])
    # from_id = int(data[1])
    # to_id = int(data[2])
    content = data[3]
    if content[0:3] == "ADD":
        music.play(music.BA_DING, wait=False)
        DOORS.add(int(content[4:]))
    elif content[0:3] == "DEL" and int(content[4:]) in DOORS:
        music.play(music.BA_DING, wait=False)
        DOORS.remove(int(content[4:]))
    elif content[:4] == "HELP":
        display.show(content[5:])
        while True:
            alert_sound()
            if button_a.was_pressed():
                display.clear()
                break

def communicate(msg):
    global ID_MB
    global received_messages
    global sent_messages

    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    if from_id == ID_MB or msg in received_messages or (
        content[0:4] == "OPEN" and to_id != ID_MB and content[:12] != "OPEN_SUCCESS"
    ):
        return

    if to_id == ID_MB:
        if content[0:3] != "ACK":
            process_action(msg)
            send_message(generate_msg_id(), to_id, from_id, "ACK_" + str(id_msg))
        else:
            id = content[4:]
            for m in sent_messages:
                if id == m[0:6]:
                    sent_messages.remove(m)
                    break
        return

    if content[0:3] != "ACK":
        for m in received_messages:
            if "ACK" == m[-10:-7] and id_msg == m[-6:]:
                return

    received_messages.append(msg)
    send_message(id_msg, from_id, to_id, content)

def check_input():
    global ID_MB
    global destino
    if button_a.was_pressed():
        destino += 1
        if destino == 13:
            destino = 0
        display.scroll(str(destino), delay=50)
    if button_b.was_pressed():
        ID_MB += 1
        if ID_MB == 13:
            ID_MB = 0
        display.scroll(str(ID_MB), delay=50)

def alert_sound():
    freq = 1850
    duration = 200
    for i in range(2):
        music.pitch(freq, duration, pin0, True)
        music.pitch(freq, duration, pin0, True)
        music.pitch(freq, duration, pin0, True)
        music.pitch(freq, duration, pin0, True)
        sleep(250)

def main():
    while True:
        display.show(Image.HEART_SMALL)
        sleep(500)
        display.clear()
        sleep(500)
        check_input()
        message = radio.receive()
        if message:
            display.show(Image.YES)
            communicate(message)

if __name__ == "__main__":
    main()
