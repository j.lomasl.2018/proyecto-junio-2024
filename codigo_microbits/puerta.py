from microbit import *
import radio
import music
import random

radio.on()
radio.config(channel=5, power=1)  # Ajusta la potencia de transmisión para asegurar una buena señal

ID_MB = 0  # Inicia el id en 0
received_messages = []  # Lista de strings
sent_messages = []  # Lista de strings

def generate_msg_id():
    return random.randint(100000, 999999)

def send_message(id_msg, from_id, to_id, content):
    message = "{} {} {} {}".format(id_msg, from_id, to_id, content)
    sent_messages.append(message)
    radio.send(message)

def process_action(msg):
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    if content[:7] == "OPENREQ":  # Solicitud de abrir puerta
        display.show(Image.YES)
        sleep(500)
        display.clear()
        handle_open_request(from_id)
    elif content[:4] == "OPEN":  # Enviar log de apertura de puerta a bs
        send_message(generate_msg_id(), ID_MB, 0, "OPEN_SUCCESS_" + str(from_id))

def communicate(msg):
    global ID_MB
    global received_messages
    global sent_messages
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    # Ignore messages sent from this device or duplicate messages
    if from_id == ID_MB or msg in received_messages:
        return

    # Process message if it's addressed to this device or it's a broadcast
    if to_id == ID_MB or to_id == 0:
        if content[:3] != "ACK":
            process_action(msg)
            send_message(generate_msg_id(), ID_MB, from_id, "ACK_" + str(id_msg))
        else:
            ack_id = int(content[4:])
            sent_messages = [m for m in sent_messages if int(m.split(" ")[0]) != ack_id]
        return

    # Relay the message if it's not an ACK and hasn't been acknowledged
    if content[:3] != "ACK":
        for m in received_messages:
            if "ACK" in m and id_msg == int(m.split(" ")[0]):
                return

    received_messages.append(msg)
    radio.send(msg)

def check_input():
    global ID_MB
    if button_a.was_pressed():
        display.scroll(str(ID_MB), delay=50)
    if button_b.was_pressed():
        ID_MB += 1
        if ID_MB == 13:
            ID_MB = 0
        display.scroll(str(ID_MB), delay=50)

def handle_open_request(id_user):
    send_message(generate_msg_id(), ID_MB, id_user, "OPENRES")

def main():
    rssi_th = -60  # Ajusta el umbral para detección cercana

    while True:
        check_input()
        received_packet = radio.receive_full()
        if received_packet:
            message, rssi, timestamp = received_packet
            message = str(message[3:], 'UTF-8')
            display.scroll(str(rssi))  # Depuración: muestra el valor de RSSI
            if rssi > rssi_th:  # Solo procesa si el RSSI es mayor que el umbral
                display.scroll("MSG")  # Depuración: indica que se recibió un mensaje
                process_action(message)

if __name__ == "__main__":
    main()
