PROJECT BOOK (P)

1. Roles And Personnel:
		Alumno				Correo
	Adrián Cabello Garrido		a.cabellog.2017@alumnos.urjc.es
	Jesús Lomas López		j.lomasl.2018@alumnos.urjc.es
	Jose Luis Bozosa Blanco		jl.bozosa@alumnos.urjc.es
	Blanca Vallés Gónzalez		b.valles.2017@alumnos.urjc.es
	
2. Imposed Technical Choice:
- Python como lenguaje de programación
- Flask para la API REST
- GitLab para gestión y organización del proyecto
- SQLite3 como lenguaje de programación para la base de datos
- Tolerancia a fallos (Alarmas)
- Micro:Bit

3. Schedule And Milestones:
- **Abrir Puertas:** Facilita a los ancianos el acceso a diferentes áreas sin necesidad de recordar o conocer IDs específicos de las puertas.
- **Localizar Ancianos:** Utiliza la red de MicroBits para identificar la ubicación de un anciano en caso de emergencia, como una caída.
- **Socorrer Ancianos:** Implica una serie de pasos interconectados y el uso de tecnología para garantizar una respuesta rápida y efectiva a situaciones de emergencia, como una caída.
- **Implementación de Repetidores Microbit:** Mejora la comunicación dentro de las habitaciones distribuyendo microbits adicionales en puntos estratégicos y ajustando la potencia de transmisión para crear una red de repetidores efectiva.
- **Mejora en el Rastreo de Ubicación Mediante Estaciones Base:** Actualiza el sistema de base de datos para identificar con precisión la ubicación específica de los residentes cuando envían una señal de auxilio.

4. Tasks And Deliverables:
Los cuatro integrantes del grupo nos hemos encargado del desarrollo de toda esta tecnología que permita a los enfermeros estar avisados en todo momento de cualquier problema que ocurra con un anciano. A su vez, se ha desarrollado una Estación Base la cual recibe todos los mensajes.

5. Risks And Mitigation Analysis:
Debemos tener cuidado con algunos de los posibles riesgos y como podrían ser resueltos:
- **Caída de la Web de una Base Station (BS):** El sistema tiene que ser capaz de, aunque una BS se haya caído, los mensajes acaben llegando a las demás.

6. Requirements Process And Reports:
El sistema que nos ha pedido el cliente tiene estas características:
- **Almacena información acerca de los pacientes:** Se hará mediante una base de datos de SQLite donde tendremos toda la información bien estructurada en tablas para poder localizarla.
- **El sistema contará con una serie de microbits que funcionarán como repetidores de señal, como transmisores y receptores de mensajes.**
- **Deberemos hacer un cliente/servidor en Python y hacer uso del MU-Editor para programar los microbits.**
- **Uso de la herramienta GitLab:** Tendremos repositorios compartidos con los demás miembros donde iremos guardando todos los avances del proyecto.

